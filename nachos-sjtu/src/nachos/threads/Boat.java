package nachos.threads;
import nachos.ag.BoatGrader;

public class Boat
{
    /*
      NOTE: island B := molokai, island A := oahu

      Our general algorithm is to ferry all the children from A to B by having two children row together from A to B and only one rowing back to A. After all of the children go, then one rows back to A, wakes up an adult. The adult rows to B, wakes up a child, and then is done. The child on B who woke up, rows to A, takes both children from A to B, and then one child rows back and repeats the process if there is an adult waiting to go. When it reaches to point where there are no more adults on A, the child rows back to B and finishes.

      Because adults never row at first (because there are always 2 children on A at the beginning)
     */

    static BoatGrader bg;

    private static int childrenOnOahu, childrenOnMolokai; 
    private static int adultsOnOahu, adultsOnMolokai;
    private static int childrenWaitBoatOahu;
    private static int childrenGoBack;
    private static boolean boatOnOahu;
    private static Lock oahuLock = new Lock();
    private static Lock molokaiLock = new Lock();
    private static Condition adultOahu = new Condition(oahuLock);
    private static Condition childrenMolokai = new Condition(molokaiLock);    
    private static Condition childrenOahu = new Condition(oahuLock);
    private static Condition childrenReadyToGo = new Condition(oahuLock);
    private static Semaphore done = new Semaphore(0);
    
    public static void selfTest()
    {
    		BoatGrader b = new BoatGrader();

	
    		// System.out.println("\n ***Testing Boats with only 2 children***");
    		// begin(0, 2, b);

    		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
    		// begin(1, 2, b);

    		//System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
    		//begin(3, 3, b);
    }

    public static void begin( int adults, int children, BoatGrader b )
    {
	// Store the externally generated autograder in a class
	// variable to be accessible by children.

	// Instantiate global variables here
    	bg = b;
    	childrenOnOahu = children; adultsOnOahu = adults;
	childrenWaitBoatOahu = 0;
	childrenGoBack = 0;
	boatOnOahu = true;
	
	// Create threads here. See section 3.4 of the Nachos for Java
	// Walkthrough linked from the projects page.
	for (int i = 0; i < children; i++) {
	    new KThread(new Runnable () { 
	    		public void run() { 
	    			ChildItinerary(); 
	    	} }).setName("c" + i).fork();
	}

	for (int i = 0; i < adults; i++) {
	    new KThread(new Runnable () { 
	    		public void run() { 
	    			AdultItinerary(); 
	    	} }).setName("a" + i).fork();
	}

	done.P();

	/*
	  Runnable r = new Runnable() {
	  public void run() {
	  SampleItinerary();
	  }
	  };
	  KThread t = new KThread(r);
	  t.setName("Sample Boat Thread");
	  t.fork();
	*/

    }

    static void AdultItinerary()
    {
		/* This is where you should put your solutions. Make calls
		   to the BoatGrader to show that it is synchronized. For
		   example:
		   bg.AdultRowToMolokai();
		   indicates that an adult has rowed the boat across to Molokai
		*/
    		oahuLock.acquire();
		while (childrenOnOahu > 1 || !boatOnOahu) adultOahu.sleep();
		adultsOnOahu--;
		boatOnOahu = false;
		oahuLock.release();
		
		bg.AdultRowToMolokai();
		
		molokaiLock.acquire();
		adultsOnMolokai++;
		childrenMolokai.wake();
		molokaiLock.release();
    }

    static void ChildItinerary(){
    		while (childrenOnOahu + adultsOnOahu > 1) {
    			oahuLock.acquire();
    			if (childrenOnOahu == 1) adultOahu.wake();
		    while (childrenWaitBoatOahu >= 2 || !boatOnOahu) childrenOahu.sleep();
		    if (childrenWaitBoatOahu == 0){ 
				childrenWaitBoatOahu++;
				childrenOahu.wake();
				childrenReadyToGo.sleep();
				bg.ChildRideToMolokai();
				childrenReadyToGo.wake();
		    } else { 
				childrenWaitBoatOahu++;
				childrenReadyToGo.wake();
				bg.ChildRowToMolokai();
				childrenReadyToGo.sleep();
		    }
		    childrenWaitBoatOahu--;
		    childrenOnOahu--;
		    boatOnOahu = false;
		    oahuLock.release();
		    
		    molokaiLock.acquire();
		    childrenOnMolokai++;
		    childrenGoBack++;
		    if (childrenGoBack == 1)  childrenMolokai.sleep();
		    childrenOnMolokai--;
		    childrenGoBack = 0;
		    molokaiLock.release();
		    
		    bg.ChildRowToOahu();
		    oahuLock.acquire();
		    childrenOnOahu++;
		    boatOnOahu = true;
		    oahuLock.release();
		}
		oahuLock.acquire();
		childrenOnOahu--;
		oahuLock.release();
		
		bg.ChildRowToMolokai();
		molokaiLock.acquire();
		childrenOnMolokai++;
		molokaiLock.release();
		done.V();
    }

    static void SampleItinerary()
    {
	// Please note that this isn't a valid solution (you can't fit
	// all of them on the boat). Please also note that you may not
	// have a single thread calculate a solution and then just play
	// it back at the autograder -- you will be caught.
	System.out.println("\n ***Everyone piles on the boat and goes to Molokai***");
	bg.AdultRowToMolokai();
	bg.ChildRideToMolokai();
	bg.AdultRideToMolokai();
	bg.ChildRideToMolokai();
    }
    
}