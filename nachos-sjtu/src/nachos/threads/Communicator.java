package nachos.threads;

import java.util.LinkedList;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		speaker = new LinkedList<ConditionS>();
		listener = new LinkedList<ConditionL>();
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		comLock.acquire();
		if (listener.isEmpty()){
			Condition cur = new Condition(comLock);
			speaker.add(new ConditionS(cur, word));
			cur.sleep();
		}else{
			ConditionL cur = listener.getFirst();
			cur.res = word;
			listener.removeFirst();
			cur.condition.wake();
		}
		comLock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		int res = -1;
		ConditionL cur;
		
		comLock.acquire();
		if (speaker.isEmpty()){
			ConditionL tmp = new ConditionL(new Condition(comLock),-1);
			listener.add(tmp);
			tmp.condition.sleep();
			res = tmp.res;
		}else{
			ConditionS spkr = speaker.getFirst();
			res = spkr.word;
			speaker.removeFirst();
			spkr.condition.wake();
		}
		comLock.release();
		return res;
	}
	
	private class ConditionS{
		Condition condition;
		int word;
		ConditionS(Condition c, int w){
			condition = c; word = w;
		}
	}
	
	private class ConditionL{
		Condition condition;
		int res;
		ConditionL(Condition c, int r){
			condition = c; res = r;
		}
	}

	private Lock comLock = new Lock();
	private LinkedList<ConditionS> speaker;
	private LinkedList<ConditionL> listener;
}
