package nachos.vm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;


public class PageScheduler {
	public PageScheduler(){
		swapFile = new SwapFile();
		pageTable = new InvertedPageTable();
	}
	
	public void init(){
		queue.clear();
		int len = Machine.processor().getNumPhysPages();
		for (int i = 0; i < len; i ++) queue.add(new Integer(i));
		swapFile.init();
		pageLock = new Lock();
	}
	
	
	private LinkedList<Integer> queue = new LinkedList<Integer>();
	
	private int getVictim(){
		while (true){
			int ppn = queue.removeFirst();
			TranslationEntry entry = pageTable.getTranslationEntry(ppn);
			if (entry == null || !entry.used) return ppn;
			entry.used = false;
			int processID = pageTable.getProcessID(ppn);
			pageTable.put(processID, entry);
			queue.add(new Integer(ppn));
		}
	}
	
	public void clearPage(int processID){
		pageTable.removeProcessPage(processID);
		swapFile.clearPage(processID);
	}
	
	public void writePageEntry(int processID, TranslationEntry entry){
		pageTable.put(processID,entry);
	}
	
	public TranslationEntry getPageEntry(MyLoader loader, int processID, int vpn){
		TranslationEntry entry = pageTable.getTranslationEntry(processID, vpn);
		if (entry == null){
			handlePageFault(loader, processID, vpn);
			entry = pageTable.getTranslationEntry(processID, vpn);
		}
		return entry;
	}
	
	public boolean handlePageFault(MyLoader loader, int processID, int vpn){
		pageLock.acquire();
		int tppn = getVictim();
		int tpid = pageTable.getProcessID(tppn); 
		int tvpn = pageTable.getVPN(tppn);
		
		if (tpid == processID) VMKernel.tlbScheduler.clear(tpid, tvpn);
		TranslationEntry entry = pageTable.getTranslationEntry(tppn);
		if (entry != null) swapFile.toFile(tpid, tvpn, entry);
		pageTable.removePage(tpid, tvpn);
		
		int ppn = tppn;
		entry = swapFile.toMemory(processID, vpn, ppn);
		if (entry == null){
			entry = new TranslationEntry(vpn, ppn, true, false, false, false);
			entry.readOnly = loader.loadSection(vpn, ppn).readOnly;
		}
		pageTable.put(processID, entry);
		queue.add(new Integer(ppn));
		pageLock.release();
		return true;
	}
	
	public InvertedPageTable pageTable;
	
	private class InvertedPageTable{
		public InvertedPageTable(){
			int len = Machine.processor().getNumPhysPages();
			pidMap = new int[len]; Arrays.fill(pidMap, -1);
			vpnMap = new int[len]; Arrays.fill(vpnMap, -1);
			entryMap = new TranslationEntry[len]; 
			mapping = new HashMap<PidVPNPair, Integer>();
		}
		
		public TranslationEntry getTranslationEntry(Integer ppn){
			if (ppn == null || ppn < 0 || ppn >= entryMap.length) return null;
			return entryMap[ppn];
		}
		
		public TranslationEntry getTranslationEntry(int processID, int vpn){
			return getTranslationEntry(mapping.get(new PidVPNPair(processID, vpn)));
		}
		
		public void put(int processID, TranslationEntry entry){
			pidMap[entry.ppn] = processID;
			vpnMap[entry.ppn] = entry.vpn;
			entryMap[entry.ppn] = entry;
			mapping.put(new PidVPNPair(processID, entry.vpn), new Integer(entry.ppn));
		}
		
		public int getProcessID(Integer ppn){
			if (ppn == null || ppn < 0 || ppn >= pidMap.length) return -1;
			return pidMap[ppn];
		}
		
		public int getVPN(Integer ppn){
			if (ppn == null || ppn < 0 || ppn >= vpnMap.length) return -1;
			return vpnMap[ppn];
		}
		
		public void removeProcessPage(int processID){
			for (int i = 0; i < pidMap.length; ++i)
				if (pidMap[i] == processID) removePage(processID, vpnMap[i]);
		}
		
		public void removePage(int processID, int vpn){
			PidVPNPair cur = new PidVPNPair(processID, vpn);
			if (mapping.containsKey(cur)){
				int ppn = mapping.remove(cur);
				pidMap[ppn] = -1; vpnMap[ppn] = -1; entryMap[ppn] = null;
			}
		}
		
		public void removePage(int ppn){
			removePage(pidMap[ppn], vpnMap[ppn]);
		}
		
		private int[] pidMap; //ppn -> pid
		private int[] vpnMap; //ppn -> vpn
		private TranslationEntry[] entryMap; //ppn -> entry
		private HashMap<PidVPNPair, Integer> mapping; //(pid, vpn) -> ppn
		
		
	}
	
	public OpenFile getSwapFile(){
		return swapFile.getFile();
	}
	private Lock pageLock;
	public SwapFile swapFile;
}
