package nachos.vm;

import sun.swing.SwingUtilities2.Section;
import nachos.machine.Coff;
import nachos.machine.CoffSection;
import nachos.machine.TranslationEntry;

public class MyLoader {
	public MyLoader(){}
	
	public MyLoader(Coff c){
		coff = c;
		int sectionCnt = coff.getNumSections();
		pageCnt = 0;
		for (int i = 0; i < sectionCnt; i ++) pageCnt += coff.getSection(i).getLength();
		pageSectionNo = new int[pageCnt];
		pageSectionOffset = new int[pageCnt];
		
		for (int i = 0; i < sectionCnt; i ++){
			CoffSection section = coff.getSection(i);
			int len = section.getLength();
			for (int j = 0; j < len; j ++){
				int vpn = section.getFirstVPN() + j;
				pageSectionNo[vpn] = i;
				pageSectionOffset[vpn] = j;
			}
		}
	}
	
	public TranslationEntry loadSection(int vpn, int ppn){
		TranslationEntry entry;
		if (vpn >= 0 && vpn < pageCnt){
			CoffSection sec = coff.getSection(pageSectionNo[vpn]);
			entry = new TranslationEntry(vpn, ppn, true, sec.isReadOnly(), false, false);
			sec.loadPage(pageSectionOffset[vpn], ppn);
		} else entry = new TranslationEntry(vpn, ppn, true, false, false, false);
		return entry;
	}
	private Coff coff;
	private int pageCnt;
	private int sectionCnt;
	private int[] pageSectionNo;
	private int[] pageSectionOffset;
}
