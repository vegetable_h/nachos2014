package nachos.vm;

public class PidVPNPair {
	public PidVPNPair(){pid = vpn = -1;}
	public PidVPNPair(int p, int v){pid = p; vpn = v;}
	@Override
	public int hashCode(){
		return pid * 111111 + vpn;
	}
	@Override
	public boolean equals(Object o){
		if (!(o instanceof PidVPNPair)) return false;
		return ((PidVPNPair)o).pid == pid && ((PidVPNPair)o).vpn == vpn;
	}
	int pid;
	int vpn;
}
