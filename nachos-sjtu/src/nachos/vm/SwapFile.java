package nachos.vm;

import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.Config;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.ThreadedKernel;

public class SwapFile {
	public SwapFile(){
		filename = "SWAP";
		mapping = new HashMap<PidVPNPair, TranslationEntry>();
		used = new HashMap<PidVPNPair, Integer>();
		available = new LinkedList<Integer>();
	}
	
	public void init(){
		pageSize = Machine.processor().pageSize;
		swapFile = ThreadedKernel.fileSystem.open(filename, true);
	}
	
	public void clearPage(int processID){
		PidVPNPair[] keys = new PidVPNPair[1];
		keys = used.keySet().toArray(keys);
		for (int i = 0; i < keys.length; i ++)
			if (keys[i] != null && keys[i].pid == processID){
				mapping.remove(keys[i]); available.add(used.remove(keys[i]));
			}
	}
	
	public void close(){
		swapFile.close();
		ThreadedKernel.fileSystem.remove(filename);
	}
	
	public int toFile(int processID, int vpn, TranslationEntry entry){
		if (entry == null) return 0;
		PidVPNPair p = new PidVPNPair(processID, vpn);
		int page;
		if (used.containsKey(p)) page = used.get(p); else page = allocate();
		mapping.put(p, entry); used.put(p, new Integer(page));
		return swapFile.write(calcOffset(page), Machine.processor().getMemory(), Processor.makeAddress(entry.ppn, 0), pageSize);
	}
	
	public TranslationEntry toMemory(int processID, int vpn, int ppn){
		PidVPNPair p = new PidVPNPair(processID, vpn);
		if (!mapping.containsKey(p)) return null;
		int page = used.get(p);
		int len = swapFile.read(calcOffset(page), Machine.processor().getMemory(), Processor.makeAddress(ppn, 0), pageSize);
		if (len < pageSize) return null;
		TranslationEntry entry = mapping.get(p);
		entry.vpn = vpn; entry.ppn = ppn; entry.valid = true; entry.dirty = false; entry.used = false;
		return entry;
	}
	
	private int calcOffset(int page){
		return page * pageSize;
	}
	
	private int allocate(){
		if (available.isEmpty()) return pageCnt ++;
		return available.removeFirst();
	}
	
	public OpenFile getFile(){return swapFile;}
	
	private HashMap<PidVPNPair, TranslationEntry> mapping;
	private HashMap<PidVPNPair, Integer> used;
	private LinkedList<Integer> available;
	
	private int pageCnt = 0;
	private String filename;
	private static int pageSize;
	OpenFile swapFile;
}
