package nachos.vm;

import nachos.machine.Machine;
import nachos.machine.TranslationEntry;

public class TLBScheduler {
	public TLBScheduler(){}
	
	public void init(){
		tlbSize = Machine.processor().getTLBSize();
		lru = new long[tlbSize]; pid = new int[tlbSize];
		for (int i = 0; i < tlbSize; i ++){
			lru[i] = Machine.timer().getTime(); pid[i] = -1;
		}
	}
	
	public void clear(int processID, int vpn){
		boolean intStatus = Machine.interrupt().disable();
		TranslationEntry empty = new TranslationEntry();
		empty.valid = false;
		for (int i = 0; i < tlbSize; i ++){
			TranslationEntry curEntry = Machine.processor().readTLBEntry(i);
			if (pid[i] == processID && curEntry.vpn == vpn){
				writeBackTLBEntry(processID, i);
				writeTLBEntry(i, empty);
			}
		}
		Machine.interrupt().setStatus(intStatus);
	}
	
	public void clearTLB(int processID){
		boolean intStatus = Machine.interrupt().disable();
		TranslationEntry empty = new TranslationEntry();
		empty.valid = false;
		for (int i = 0; i < tlbSize; i ++){
			writeBackTLBEntry(processID, i); writeTLBEntry(i, empty);
		}
		Machine.interrupt().setStatus(intStatus);
	}
	
	public void addTLBEntry(int processID, TranslationEntry entry){
		boolean intStatus = Machine.interrupt().disable();
		int vic = getVictim();
		writeBackTLBEntry(processID, vic); writeTLBEntry(vic, entry);
		pid[vic] = processID; lru[vic] = Machine.timer().getTime();
		Machine.interrupt().setStatus(intStatus);
	}
	
	public void writeTLBEntry(int cur, TranslationEntry entry){
		boolean intStatus = Machine.interrupt().disable();
		Machine.processor().writeTLBEntry(cur, entry);
		Machine.interrupt().setStatus(intStatus);
	}
	
	public void writeBackTLBEntry(int processID, int cur){
		boolean intStatus = Machine.interrupt().disable();
		TranslationEntry entry = Machine.processor().readTLBEntry(cur);
		if (entry.dirty) writePageEntry(processID, entry);
		Machine.interrupt().setStatus(intStatus);
	}
	
	public void writePageEntry(int processID, TranslationEntry entry){
		VMKernel.pageScheduler.writePageEntry(processID, entry);
	}
	
	public TranslationEntry getPageEntry(MyLoader loader, int processID, int vpn){
		return VMKernel.pageScheduler.getPageEntry(loader, processID, vpn);
	}
	
	public boolean handleTLBMiss(MyLoader loader, int processID, int vpn){
		TranslationEntry entry = getPageEntry(loader, processID, vpn);
		if (entry == null) return false;
		addTLBEntry(processID, entry);
		return true;
	}
	
	private int getVictim(){
		long earliest = lru[0]; int cur = 0;
		for (int i = 1; i < tlbSize; i ++)
			if (lru[i] < earliest){earliest = lru[i]; cur = i;}
		return cur;
	}
	
	public static int tlbSize;
	private int[] pid;
	private long[] lru;
}
